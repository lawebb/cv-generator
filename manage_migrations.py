from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import app, db, models

# Initiating the migration instance so that the database can be migrated
# without having to be recreated.

migrate = Migrate(app, db)

manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
