from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Content
from app.letters.forms import AddContentForm, EditContentForm


# Initialising the letters blueprint.
letters = Blueprint('letters', __name__)


# The route for displaying, editing and creating letter content.

@letters.route("/letter-content", methods=['GET', 'POST'])
def content():

    form=AddContentForm()
    eform=EditContentForm()

    content = Content.query.all()

    if request.method == 'POST':

        if request.form['submit_button'] == "Add Content" and form.validate_on_submit():


            content = Content(name=form.name.data,
                            description=form.description.data)

            db.session.add(content)
            db.session.commit()

            flash('Successfully added content', 'success')
            return redirect(url_for('letters.content'))

        elif request.form['submit_button'] == "Save Changes" and form.validate_on_submit():

            content = Content.query.filter(Content.id == eform.id.data).first()

            content.name = eform.name.data
            content.description = eform.description.data

            db.session.commit()

            flash('Successfully updated content', 'success')
            return redirect(url_for('letters.content'))


    return render_template('letters.html', title='Letter Content', form=form, eform=eform, content=content)


# The route for deleting letter content.

@letters.route("/delete-content/<id>")
def deleteContent(id):

    content = Content.query.filter(Content.id == id).first()

    db.session.delete(content)
    db.session.commit()

    flash('Successfully deleted content', 'success')
    return redirect(url_for('letters.content'))


# The route for activating letter content.

@letters.route("/activate-content/<id>")
def activateContent(id):

    content = Content.query.filter(Content.id == id).first()

    content.active = 1
    db.session.commit()

    flash('Successfully activated content', 'success')
    return redirect(url_for('letters.content'))


# The route for inactivating letter content.

@letters.route("/inactivate-content/<id>")
def inactivateContent(id):

    content = Content.query.filter(Content.id == id).first()

    content.active = 0
    db.session.commit()

    flash('Successfully inactivated content', 'success')
    return redirect(url_for('letters.content'))