from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SelectField, DateField, TextAreaField
from wtforms.validators import InputRequired

class AddContentForm(FlaskForm):
    name = StringField('Name:', validators=[InputRequired()])
    description = TextAreaField('Letter content:', validators=[InputRequired()], render_kw={'rows': 20})

class EditContentForm(FlaskForm):
    id = IntegerField('Experience ID:', validators=[InputRequired()], render_kw={'readonly': True})
    name = StringField('Name:', validators=[InputRequired()])
    description = TextAreaField('Letter content:', validators=[InputRequired()], render_kw={'rows': 20})