from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SelectField, DateField, TextAreaField
from wtforms.validators import InputRequired

class AddEducationForm(FlaskForm):
    school = StringField('School:', validators=[InputRequired()])
    course = StringField('Course:', validators=[InputRequired()])
    confirmed = SelectField('Grade Confirmed or Expected?:', choices=["Confirmed", "Expected"],
        default="Confirmed", validators=[InputRequired()])
    grade = StringField('Grade:', validators=[InputRequired()])
    start_date = DateField('Start Date (dd/mm/yyyy):', format='%Y-%m-%d', validators=[InputRequired()])
    end_date = DateField('End Date (dd/mm/yyyy):', format='%Y-%m-%d', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})

class EditEducationForm(FlaskForm):
    id = IntegerField('Education ID:', validators=[InputRequired()], render_kw={'readonly': True})
    school = StringField('School:', validators=[InputRequired()])
    course = StringField('Course:', validators=[InputRequired()])
    confirmed = SelectField('Grade Confirmed or Expected?:', choices=["Confirmed", "Expected"],
        default="Confirmed", validators=[InputRequired()])
    grade = StringField('Grade:', validators=[InputRequired()])
    start_date = DateField('Start Date (dd/mm/yyyy):', format='%Y-%m-%d', validators=[InputRequired()])
    end_date = DateField('End Date (dd/mm/yyyy):', format='%Y-%m-%d', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})