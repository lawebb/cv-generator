from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Education
from app.educations.forms import AddEducationForm, EditEducationForm


# Initialising the education blueprint.
educations = Blueprint('educations', __name__)


# The route for displaying, editing and creating education.

@educations.route("/education", methods=['GET', 'POST'])
def education():

    form=AddEducationForm()
    eform=EditEducationForm()

    education = Education.query.all()

    education.sort(key=lambda x: x.end_date, reverse=True)

    if request.method == 'POST':

        if request.form['submit_button'] == "Add Education" and form.validate_on_submit():

            if form.grade.data:
                education = Education(school=form.school.data,
                                    course=form.course.data,
                                    grade=form.grade.data,
                                    start_date=form.start_date.data,
                                    end_date=form.end_date.data,
                                    description=form.description.data)
            else:
                education = Education(school=form.school.data,
                                    course=form.course.data,
                                    expected_grade=form.expected_grade.data,
                                    start_date=form.start_date.data,
                                    end_date=form.end_date.data,
                                    description=form.description.data)

            db.session.add(education)
            db.session.commit()

            flash('Successfully added education', 'success')
            return redirect(url_for('educations.education'))

        elif request.form['submit_button'] == "Save Changes" and form.validate_on_submit():

            education = Education.query.filter(Education.id == eform.id.data).first()

            if eform.confirmed.data == "Confirmed":
                education.school = eform.school.data
                education.course = eform.course.data
                education.grade = eform.grade.data
                education.expected_grade = None
                education.start_date = eform.start_date.data
                education.end_date = eform.end_date.data
                education.description = eform.description.data
            
            else:
                education.school = eform.school.data
                education.course = eform.course.data
                education.grade = None
                education.expected_grade = eform.grade.data
                education.start_date = eform.start_date.data
                education.end_date = eform.end_date.data
                education.description = eform.description.data

            db.session.commit()

            flash('Successfully updated education', 'success')
            return redirect(url_for('educations.education'))


    return render_template('education.html', title='Education', form=form, eform=eform, education=education)


# The route for deleting education.

@educations.route("/delete-education/<id>")
def deleteEducation(id):

    education = Education.query.filter(Education.id == id).first()

    db.session.delete(education)
    db.session.commit()

    flash('Successfully deleted education', 'success')
    return redirect(url_for('educations.education'))


# The route for actviating education.

@educations.route("/activate-education/<id>")
def activateEducation(id):

    education = Education.query.filter(Education.id == id).first()

    education.active = 1
    db.session.commit()

    flash('Successfully activated education', 'success')
    return redirect(url_for('educations.education'))


# The route for inactivating education.

@educations.route("/inactivate-education/<id>")
def inactivateEducation(id):

    education = Education.query.filter(Education.id == id).first()

    education.active = 0
    db.session.commit()

    flash('Successfully inactivated education', 'success')
    return redirect(url_for('educations.education'))