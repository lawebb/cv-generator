from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app.models import Cv, Details, Content, Letter, Summary, Experience, Education, Interest, Skill, Project
from app.cvs.forms import AddCvForm

from app import db

from PyPDF2 import PdfFileReader
import pdfkit
import shutil
import os.path

from os import path

# Initialising the cvs blueprint.
cvs = Blueprint('cvs', __name__)


# The route for displaying, editing and creating summaries.

@cvs.route("/", methods=['GET', 'POST'])
@cvs.route("/cvs", methods=['GET', 'POST'])
def cv():

    form=AddCvForm()

    cv=Cv.query.all()

    detail = Details.query.filter(Details.id == 1).first()
    summary = Summary.query.filter(Summary.active == 1).first()
    experience = Experience.query.filter(Experience.active == 1).all()
    education = Education.query.filter(Education.active == 1).all()
    project = Project.query.filter(Project.active == 1).all()
    skill = Skill.query.filter(Skill.active == 1).all()
    interest = Interest.query.filter(Interest.active == 1).all()

    for c in cv:
        filename=c.name.replace(" ", "_")

        if path.exists("app/static/cvs/" + filename + ".pdf"):
            c.generated = 1

            db.session.commit()
        else:
            c.generated = 0

            db.session.commit()

        if path.exists("app/static/letters/" + filename + "_letter.pdf"):
            c.letter.generated = 1

            db.session.commit()
        else:
            c.letter.generated = 0

            db.session.commit()

    if request.method=='POST':

        if request.form['submit_button'] == 'Add CV' and form.validate_on_submit():

            content = Content.query.filter(Content.active == 1).first()

            letter = Letter(content=content)

            db.session.add(letter)
            db.session.commit()

            cv = Cv(name=form.name.data,
                    details_id=detail.id,
                    letter=letter)
            
            db.session.add(cv)
            db.session.commit()

            cv.summary = summary

            for e in experience:
                if e.active == 1:
                    cv.experience.append(e)

            for e in education:
                if e.active == 1:
                    cv.education.append(e)

            for p in project:
                if p.active == 1:
                    cv.projects.append(p)

            for s in skill:
                if s.active == 1:
                    cv.skills.append(s)

            for i in interest:
                if i.active == 1:
                    cv.interests.append(i)
        

            db.session.commit()

            flash('Successfully added CV', 'success')
            return redirect(url_for('cvs.cv'))

    return render_template('cv.html', title = 'CVs', form=form, cv=cv, detail=detail, experience=experience, education=education,
        skill=skill, interest=interest, project=project, summary=summary)



# The route for generating a PDF of a CV.

@cvs.route("/generate-cv/<id>")
def generateCv(id):

    c = Cv.query.filter(Cv.id == id).first()

    exp = c.experience
    exp.sort(key=lambda x: x.end_date, reverse=True)

    edu = c.education
    edu.sort(key=lambda x: x.end_date, reverse=True)

    proj= c.projects
    proj.sort(key=lambda x: x.id)

    css=['app/static/css/bootstrap.min.css', 'app/static/css/circle.css']

    
    filename= c.name + '.pdf'

    filename=filename.replace(" ", "_")

    linkedin_url= os.getcwd() + "/app/static/images/linkedin.png"
    email_url= os.getcwd() + "/app/static/images/email.png"
    phone_url= os.getcwd() + "/app/static/images/phone.png"
    gitlab_url= os.getcwd() + "/app/static/images/gitlab.svg"
    bullet_url = os.getcwd() + "/app/static/images/circle.png"
    location_url = os.getcwd() + "/app/static/images/location.png"

    
    cv = render_template('cv-layout1.html', linkedin_url=linkedin_url, email_url=email_url, phone_url=phone_url, location_url=location_url,
        gitlab_url=gitlab_url, bullet_url=bullet_url, c=c, proj=proj, exp=exp, edu=edu, nonprogress_colour="white", progress_colour="#304145",
        writing_colour2="black", writing_colour="white", header_background_colour="#304145", side_background_colour="#a5b7c0")

    options = {
    'user-style-sheet':css,
    'encoding': 'UTF-8',        
    'margin-left': '0mm',
    'margin-right': '0mm',
    'margin-bottom': '0mm',
    'margin-top': '0mm'
    }

    pdfkit.from_string(cv, filename, options=options, css=css)
    filess = [filename]
    for f in filess:
        shutil.move(f, 'app/static/cvs')

    flash('Successfully generated CV', 'success')
    return redirect(url_for('cvs.cv'))



# The route for generating a PDF of a Letter.

@cvs.route("/generate-letter/<id>")
def generateLetter(id):

    c = Cv.query.filter(Cv.id == id).first()

    l = c.letter

    css=['app/static/css/bootstrap.min.css']
    
    filename= c.name + '_letter.pdf'

    filename=filename.replace(" ", "_")

    linkedin_url= os.getcwd() + "/app/static/images/linkedin.png"
    email_url= os.getcwd() + "/app/static/images/email.png"
    phone_url= os.getcwd() + "/app/static/images/phone.png"
    gitlab_url= os.getcwd() + "/app/static/images/gitlab.svg"
    bullet_url = os.getcwd() + "/app/static/images/circle.png"
    location_url = os.getcwd() + "/app/static/images/location.png"

    
    letter = render_template('letter-layout1.html', linkedin_url=linkedin_url, email_url=email_url, phone_url=phone_url, location_url=location_url,
        gitlab_url=gitlab_url, c=c,
        writing_colour2="black", writing_colour="white", header_background_colour="#111e2f", side_background_colour="#98979b")

    options = {
    'user-style-sheet':css,
    'encoding': 'UTF-8',        
    'margin-left': '0mm',
    'margin-right': '0mm',
    'margin-bottom': '0mm',
    'margin-top': '0mm'
    }

    pdfkit.from_string(letter, filename, options=options, css=css)
    filess = [filename]
    for f in filess:
        shutil.move(f, 'app/static/letters')

    flash('Successfully generated Letter', 'success')
    return redirect(url_for('cvs.cv'))


# The route for displaying the PDF created for a CV.

@cvs.route("/view-cv/<id>", methods=['GET'])
def viewCv(id):

    c = Cv.query.filter(Cv.id == id).first()

    filename=c.name.replace(" ", "_")

    cv_url = '/static/cvs/' + filename + '.pdf'

    return render_template('cvpdf.html', title=c.name, cv_url=cv_url)


# The route for displaying the PDF created for a Letter.

@cvs.route("/view-letter/<id>", methods=['GET'])
def viewLetter(id):

    c = Cv.query.filter(Cv.id == id).first()

    filename=c.name.replace(" ", "_")

    letter_url = '/static/letters/' + filename + '_letter.pdf'

    return render_template('letterpdf.html', title=c.name, letter_url=letter_url)


# The route for deleting a CV.

@cvs.route("/delete-cv/<id>")
def deleteCv(id):

    c = Cv.query.filter(Cv.id == id).first()

    if c.generated:
        filename=c.name.replace(" ", "_")
        os.remove("app/static/cvs/" + filename + ".pdf")

    if c.letter.generated:
        filename=c.name.replace(" ", "_")
        os.remove("app/static/letters/" + filename + "_letter.pdf")

    db.session.delete(c.letter)
    db.session.delete(c)
    db.session.commit()

    flash('Successfully deleted CV', 'success')
    return redirect(url_for('cvs.cv'))
