from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SelectField, DateField, TextAreaField
from wtforms.validators import InputRequired

class AddExperienceForm(FlaskForm):
    workplace = StringField('Workplace:', validators=[InputRequired()])
    location = StringField('Location:', validators=[InputRequired()])
    sort = SelectField('Type:', choices=["Full-time", "Part-time", "Work Experience", "Volunteering"],
        default="Full-time", validators=[InputRequired()])
    title = StringField('Title:', validators=[InputRequired()])
    start_date = DateField('Start Date (dd/mm/yyyy):', format='%Y-%m-%d', validators=[InputRequired()])
    end_date = DateField('End Date (dd/mm/yyyy) or 11/11/9999 for current:', format='%Y-%m-%d', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})

class EditExperienceForm(FlaskForm):
    id = IntegerField('Experience ID:', validators=[InputRequired()], render_kw={'readonly': True})
    workplace = StringField('Workplace:', validators=[InputRequired()])
    location = StringField('Location:', validators=[InputRequired()])
    sort = SelectField('Type:', choices=["Full-time", "Part-time", "Work Experience", "Volunteering"],
        validators=[InputRequired()])
    title = StringField('Title:', validators=[InputRequired()])
    start_date = DateField('Start Date (dd/mm/yyyy):', format='%Y-%m-%d', validators=[InputRequired()])
    end_date = DateField('End Date (dd/mm/yyyy):', format='%Y-%m-%d', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})