from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Experience
from app.experiences.forms import AddExperienceForm, EditExperienceForm


# Initialising the experiences blueprint.
experiences = Blueprint('experiences', __name__)


# The route for displaying, editing and creating experiences.

@experiences.route("/work-experience", methods=['GET', 'POST'])
def workExperience():

    form=AddExperienceForm()
    eform=EditExperienceForm()

    experience = Experience.query.all()

    experience.sort(key=lambda x: x.end_date, reverse=True)

    if request.method == 'POST':

        if request.form['submit_button'] == "Add Experience" and form.validate_on_submit():


            experience = Experience(workplace=form.workplace.data,
                                    location=form.location.data,
                                    sort=form.sort.data,
                                    title=form.title.data,
                                    start_date=form.start_date.data,
                                    end_date=form.end_date.data,
                                    description=form.description.data)

            db.session.add(experience)
            db.session.commit()

            flash('Successfully added work experience', 'success')
            return redirect(url_for('experiences.workExperience'))

        elif request.form['submit_button'] == "Save Changes" and form.validate_on_submit():

            experience = Experience.query.filter(Experience.id == eform.id.data).first()

            experience.workplace = eform.workplace.data
            experience.location = eform.location.data
            experience.sort = eform.sort.data
            experience.title = eform.title.data
            experience.start_date = eform.start_date.data
            experience.end_date = eform.end_date.data
            experience.description = eform.description.data

            db.session.commit()

            flash('Successfully updated work experience', 'success')
            return redirect(url_for('experiences.workExperience'))


    return render_template('work-experience.html', title='Work Experience', form=form, eform=eform, experience=experience)


# The route for deleting experiences.

@experiences.route("/delete-experience/<id>")
def deleteExperience(id):

    experience = Experience.query.filter(Experience.id == id).first()

    db.session.delete(experience)
    db.session.commit()

    flash('Successfully deleted work experience', 'success')
    return redirect(url_for('experiences.workExperience'))


# The route for activating experiences.

@experiences.route("/activate-experience/<id>")
def activateExperience(id):

    experience = Experience.query.filter(Experience.id == id).first()

    experience.active = 1
    db.session.commit()

    flash('Successfully activated work experience', 'success')
    return redirect(url_for('experiences.workExperience'))


# The route for inactivating experiences.

@experiences.route("/inactivate-experience/<id>")
def inactivateExperience(id):

    experience = Experience.query.filter(Experience.id == id).first()

    experience.active = 0
    db.session.commit()

    flash('Successfully inactivated work experience', 'success')
    return redirect(url_for('experiences.workExperience'))