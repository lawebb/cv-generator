from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, IntegerField
from wtforms.validators import InputRequired

class AddSummaryForm(FlaskForm):
    name = StringField('Summary Name:', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})

class EditSummaryForm(FlaskForm):
    id = IntegerField('Summary ID:', validators=[InputRequired()], render_kw={'readonly': True})
    name = StringField('Summary Name:', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})