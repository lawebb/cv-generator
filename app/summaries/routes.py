from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Summary
from app.summaries.forms import AddSummaryForm, EditSummaryForm

# Initialising the summaries blueprint

summaries = Blueprint('summaries', __name__)


# The route for displaying, editing and creating summaries.

@summaries.route("/summary", methods=['GET', 'POST'])
def summary():

    form=AddSummaryForm()
    eform=EditSummaryForm()

    summary = Summary.query.all()

    if request.method == 'POST':

        if request.form['submit_button'] == "Add Summary" and form.validate_on_submit():


            summary = Summary(name=form.name.data,
                            description=form.description.data)

            db.session.add(summary)
            db.session.commit()

            flash('Successfully added summary', 'success')
            return redirect(url_for('summaries.summary'))

        elif request.form['submit_button'] == "Save Changes" and form.validate_on_submit():

            summary = Summary.query.filter(Summary.id == eform.id.data).first()

            summary.name = eform.name.data
            summary.description = eform.description.data

            db.session.commit()

            flash('Successfully updated summary', 'success')
            return redirect(url_for('summaries.summary'))


    return render_template('summary.html', title='Summaries', form=form, eform=eform, summary=summary)


# The route for deleting summaries.

@summaries.route("/delete-summary/<id>")
def deleteSummary(id):

    summary = Summary.query.filter(Summary.id == id).first()

    db.session.delete(summary)
    db.session.commit()

    flash('Successfully deleted summary', 'success')
    return redirect(url_for('summaries.summary'))


# The route for activating summaries.

@summaries.route("/activate-summary/<id>")
def activateSummary(id):

    summary = Summary.query.filter(Summary.id == id).first()

    summary.active = 1
    db.session.commit()

    flash('Successfully activated summary', 'success')
    return redirect(url_for('summaries.summary'))


# The route for inactivating summaries.

@summaries.route("/inactivate-summary/<id>")
def inactivateSummary(id):

    summary = Summary.query.filter(Summary.id == id).first()

    summary.active = 0
    db.session.commit()

    flash('Successfully inactivated summary', 'success')
    return redirect(url_for('summaries.summary'))