from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Skill
from app.skills.forms import AddSkillForm


# Initialising the skills blueprint.
skills = Blueprint('skills', __name__)


# The route for displaying and creating skills.

@skills.route("/skills", methods=['GET', 'POST'])
def skill():

    form=AddSkillForm()

    skill = Skill.query.all()

    skill.sort(key=lambda x: x.sort, reverse=True)

    if request.method == 'POST':

        if request.form['submit_button'] == "Add Skill" and form.validate_on_submit():

            skill = Skill(name=form.name.data,
                                    sort=form.sort.data,
                                    rating=form.rating.data)

            db.session.add(skill)
            db.session.commit()

            flash('Successfully added skill', 'success')
            return redirect(url_for('skills.skill'))

    return render_template('skills.html', title = 'Skill', form=form, skill=skill)


# The route for deleting skills.

@skills.route("/delete-skill/<id>")
def deleteSkill(id):

    skill = Skill.query.filter(Skill.id == id).first()

    db.session.delete(skill)
    db.session.commit()

    flash('Successfully deleted skill', 'success')
    return redirect(url_for('skills.skill'))


# The route for activating skills.

@skills.route("/activate-skill/<id>")
def activateSkill(id):

    skill = Skill.query.filter(Skill.id == id).first()

    skill.active = 1
    db.session.commit()

    flash('Successfully activated skill', 'success')
    return redirect(url_for('skills.skill'))


# The route for inactivating skills.

@skills.route("/inactivate-skill/<id>")
def inactivateSkill(id):

    skill = Skill.query.filter(Skill.id == id).first()

    skill.active = 0
    db.session.commit()

    flash('Successfully inactivated skill', 'success')
    return redirect(url_for('skills.skill'))