from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import InputRequired

class AddSkillForm(FlaskForm):
    name = StringField('Skill Name:', validators=[InputRequired()])
    sort = SelectField('Type:', choices=["General", "Technical"],
        default="General", validators=[InputRequired()])
    rating = SelectField('Rating:', choices=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        default="1", validators=[InputRequired()])