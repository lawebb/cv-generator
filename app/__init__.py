from flask import Flask
from flask_bootstrap import Bootstrap
from config import DevelopmentConfig
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager

# Initialising app and database.

app = Flask(__name__)

app.config.from_object(DevelopmentConfig)

Bootstrap(app)

db = SQLAlchemy(app)


# Initialising the Flask Blueprints.
from app import models

from app.cvs.routes import cvs
from app.letters.routes import letters
from app.details.routes import details
from app.summaries.routes import summaries
from app.projects.routes import projects
from app.experiences.routes import experiences
from app.educations.routes import educations
from app.interests.routes import interests
from app.skills.routes import skills

app.register_blueprint(cvs)
app.register_blueprint(letters)
app.register_blueprint(details)
app.register_blueprint(summaries)
app.register_blueprint(projects)
app.register_blueprint(experiences)
app.register_blueprint(educations)
app.register_blueprint(interests)
app.register_blueprint(skills)