from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Project
from app.projects.forms import AddProjectForm, EditProjectForm


#Initialising the projects blueprint.
projects = Blueprint('projects', __name__)


# The route for displaying, editing and creating projects.

@projects.route("/project", methods=['GET', 'POST'])
def project():

    form=AddProjectForm()
    eform=EditProjectForm()

    project = Project.query.all()

    if request.method == 'POST':

        if request.form['submit_button'] == "Add Project" and form.validate_on_submit():

            project=Project(name=form.name.data,
                            description=form.description.data)

            db.session.add(project)
            db.session.commit()

            flash('Successfully added project', 'success')
            return redirect(url_for('projects.project'))

        elif request.form['submit_button'] == "Save Changes" and form.validate_on_submit():

            project = Project.query.filter(Project.id == eform.id.data).first()

            project.name = eform.name.data
            project.description = eform.description.data

            db.session.commit()

            flash('Successfully updated project', 'success')
            return redirect(url_for('projects.project'))


    return render_template('projects.html', title='Projects', form=form, eform=eform, project=project)


# The route for deleting projects.

@projects.route("/delete-project/<id>")
def deleteProject(id):

    project = Project.query.filter(Project.id == id).first()

    db.session.delete(project)
    db.session.commit()

    flash('Successfully deleted project', 'success')
    return redirect(url_for('projects.project'))


# The route for activating projects.

@projects.route("/activate-project/<id>")
def activateProject(id):

    project = Project.query.filter(Project.id == id).first()

    project.active = 1
    db.session.commit()

    flash('Successfully activated project', 'success')
    return redirect(url_for('projects.project'))


# The route for inactivating projects.

@projects.route("/inactivate-project/<id>")
def inactivateProject(id):

    project = Project.query.filter(Project.id == id).first()

    project.active = 0
    db.session.commit()

    flash('Successfully inactivated project', 'success')
    return redirect(url_for('projects.project'))