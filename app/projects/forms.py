from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, IntegerField
from wtforms.validators import InputRequired

class AddProjectForm(FlaskForm):
    name = StringField('Project Name:', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})

class EditProjectForm(FlaskForm):
    id = IntegerField('Project ID:', validators=[InputRequired()], render_kw={'readonly': True})
    name = StringField('Project Name:', validators=[InputRequired()])
    description = TextAreaField('Description:', validators=[InputRequired()], render_kw={'rows': 5})