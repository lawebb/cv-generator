from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Details
from app.details.forms import DetailsForm


# Initialising the details blueprint.
details = Blueprint('details', __name__)


# The route for displaying and editing personal details.

@details.route("/personal-details", methods=['GET', 'POST'])
def personalDetails():

    form=DetailsForm()

    details = Details.query.filter(Details.id == 1).first()

    if request.method == 'POST':

        if request.form['submit_button'] == "Save Changes" and form.validate_on_submit():

            details = Details.query.first()

            details.name = form.name.data
            details.title = form.title.data
            details.mobile = form.mobile.data
            details.email = form.email.data
            details.linkedin = form.linkedin.data
            details.gitlab = form.gitlab.data
            details.address = form.address.data

            db.session.commit()

            flash('Successfully updated personal details', 'success')
            return redirect(url_for('details.personalDetails'))
        
        elif request.form['submit_button'] == "Add Details" and form.validate_on_submit():

            details = Details(name=form.name.data,
                            title=form.title.data,
                            mobile=form.mobile.data,
                            email=form.email.data,
                            linkedin=form.linkedin.data,
                            gitlab=form.gitlab.data,
                            address=form.address.data)

            db.session.add(details)
            db.session.commit()

            flash('Successfully added personal details', 'success')
            return redirect(url_for('details.personalDetails'))

    return render_template('personal-details.html', title='Personal Details', form=form, details=details)
