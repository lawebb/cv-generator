from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import InputRequired, Email

class DetailsForm(FlaskForm):
    name = StringField('Full Name:', validators=[InputRequired()])
    title = StringField('Title:', validators=[InputRequired()])
    mobile = StringField('Mobile Number:', validators=[InputRequired()])
    email = StringField('Email:', validators=[InputRequired(), Email(message="Please enter an Email Address")])
    linkedin = StringField('LinkedIn:', validators=[InputRequired()])
    gitlab = StringField('GitLab:', validators=[InputRequired()])
    address = StringField('Address:', validators=[InputRequired()])