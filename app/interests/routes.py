from flask import render_template, redirect, request, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy

from app import db

from app.models import Interest
from app.interests.forms import AddInterestForm


#Initialising the interests blueprint.
interests = Blueprint('interests', __name__)


# The route for displaying and creating interests.

@interests.route("/interests-and-hobbies", methods=['GET', 'POST'])
def interest():

    form=AddInterestForm()

    interest = Interest.query.all()

    if request.method == 'POST':

        if request.form['submit_button'] == "Add Interest" and form.validate_on_submit():

            interest = Interest(name=form.name.data)

            db.session.add(interest)
            db.session.commit()

            flash('Successfully added interest', 'success')
            return redirect(url_for('interests.interest'))

    return render_template('interests.html', title = 'Interests & Hobbies', form=form, interest=interest)


# The route for deleting interets.

@interests.route("/delete-interest/<id>")
def deleteInterest(id):

    interest = Interest.query.filter(Interest.id == id).first()

    db.session.delete(interest)
    db.session.commit()

    flash('Successfully deleted interest', 'success')
    return redirect(url_for('interests.interest'))


# The route for activating interests.

@interests.route("/activate-interest/<id>")
def activateInterest(id):

    interest = Interest.query.filter(Interest.id == id).first()

    interest.active = 1
    db.session.commit()

    flash('Successfully activated interest', 'success')
    return redirect(url_for('interests.interest'))


# The route for inactivating interests.

@interests.route("/inactivate-interest/<id>")
def inactivateInterest(id):

    interest = Interest.query.filter(Interest.id == id).first()

    interest.active = 0
    db.session.commit()

    flash('Successfully inactivated interest', 'success')
    return redirect(url_for('interests.interest'))