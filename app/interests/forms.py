from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import InputRequired

class AddInterestForm(FlaskForm):
    name = StringField('Interest Name:', validators=[InputRequired()])