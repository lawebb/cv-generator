from datetime import datetime, time, date
from app import app, db
from wtforms.validators import ValidationError
import os.path

pro=db.Table('pro', 
    db.Column('cv_id', db.Integer, db.ForeignKey('cv.id')),
    db.Column('project_id', db.Integer, db.ForeignKey('project.id'))
    )

exp=db.Table('exp', 
    db.Column('cv_id', db.Integer, db.ForeignKey('cv.id')),
    db.Column('experience_id', db.Integer, db.ForeignKey('experience.id'))
    )

edu=db.Table('edu', 
    db.Column('cv_id', db.Integer, db.ForeignKey('cv.id')),
    db.Column('education_id', db.Integer, db.ForeignKey('education.id'))
    )

skls=db.Table('skls', 
    db.Column('cv_id', db.Integer, db.ForeignKey('cv.id')),
    db.Column('skill_id', db.Integer, db.ForeignKey('skill.id'))
    )

intrs=db.Table('intrs', 
    db.Column('cv_id', db.Integer, db.ForeignKey('cv.id')),
    db.Column('interest_id', db.Integer, db.ForeignKey('interest.id'))
    )


class Cv(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True, nullable=False)
    references = db.Column(db.String(1000), default='Can be provided on request.')
    generated = db.Column(db.Integer)

    details_id = db.Column(db.Integer, db.ForeignKey('details.id'))
    summary_id = db.Column(db.Integer, db.ForeignKey('summary.id'))
    letter_id = db.Column(db.Integer, db.ForeignKey('letter.id'))
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'))

    projects = db.relationship('Project', secondary=pro, backref=db.backref('cvs', lazy='dynamic'))
    experience = db.relationship('Experience', secondary=exp, backref=db.backref('cvs', lazy='dynamic'))
    education = db.relationship('Education', secondary=edu, backref=db.backref('cvs', lazy='dynamic'))
    skills = db.relationship('Skill', secondary=skls, backref=db.backref('cvs', lazy='dynamic'))
    interests = db.relationship('Interest', secondary=intrs, backref=db.backref('cvs', lazy='dynamic'))

    def __repr__(self):
        return "<CV(id='%s', name='%s')>" % self.id, self.name

class Letter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    generated = db.Column(db.Integer)

    content_id = db.Column(db.Integer, db.ForeignKey('content.id'))

    cvs = db.relationship('Cv', backref='letter', lazy='dynamic')

    def __repr__(self):
        return "<Letter(id='%s')>" % self.id

class Content(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    description = db.Column(db.String(1000), nullable=False)
    active = db.Column(db.Integer)

    letters = db.relationship('Letter', backref='content', lazy='dynamic')
    cvs = db.relationship('Cv', backref='content', lazy='dynamic')

    def __repr__(self):
        return "<Content(id='%s')>" % self.id

class Details(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    title = db.Column(db.String(50), nullable=False)
    mobile = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    linkedin = db.Column(db.String(50), nullable=False)
    gitlab  = db.Column(db.String(50), nullable=False)
    address = db.Column(db.String(100))

    cvs = db.relationship('Cv', backref='details', lazy='dynamic')

    def __repr__(self):
        return "<Details(id='%s', name='%s')>" % self.id, self.name

class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    description = db.Column(db.String(1000), nullable=False)
    active = db.Column(db.Integer)

    def __repr__(self):
        return "<Project(id='%s', name='%s')>" % self.id, self.name

class Summary(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    description = db.Column(db.String(1000), nullable=False)
    active = db.Column(db.Integer)

    cvs = db.relationship('Cv', backref='summary', lazy='dynamic')

    def __repr__(self):
        return "<Summary(id='%s', name='%s')>" % self.id, self.name

class Experience(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    workplace = db.Column(db.String(50), nullable=False)
    location = db.Column(db.String(50), nullable=False)
    sort = db.Column(db.String(30), nullable=False)
    title = db.Column(db.String(50), nullable=False)
    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date, nullable=False)
    description = db.Column(db.String(1000), nullable=False)
    active = db.Column(db.Integer)


    def __repr__(self):
        return "<Experience(id='%s', workplace='%s')>" % self.id, self.workplace

class Education(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    school = db.Column(db.String(50), nullable=False)
    course = db.Column(db.String(50), nullable=False)
    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date, nullable=False)
    grade = db.Column(db.String(50))
    expected_grade = db.Column(db.String(50))
    description = db.Column(db.String(1000))
    active = db.Column(db.Integer)


    def __repr__(self):
        return "<Education(id='%s', school='%s')>" % self.id, self.school

class Skill(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    sort = db.Column(db.String(30), nullable=False)
    rating = db.Column(db.String(5), nullable=False)
    active = db.Column(db.Integer)

    def __repr__(self):
        return "<Skill(id='%s')>" % self.id

class Interest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    active = db.Column(db.Integer)

    def __repr__(self):
        return "<Interest(id='%s', name='%s')>" % self.id, self.name

if os.path.exists('app.db'):
    pass
else:
    db.create_all()