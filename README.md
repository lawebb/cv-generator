# Graduate CV Generator
This tool generates a CV from data entered by the user and saved in an SQLite database. The tool is designed using Python, the Flask framework and SQLite for the backend. The front end, including the CV design itself, is designed using HTML, CSS, Javascript and the Bootstrap framework. The code used to design the CV is then exported and generated into a PDF that can be shared with employers.

# Why I made this program
I made this piece of software because I was coming up to my final year of university, where I would be required to develop CVs for employers. This can be a time consuming process and with the wave of applicants likely for each position, it can be hard to stand out from the crowd. I decided that the best way to tackle these two issues was to design a program that I could use to streamline my CV generation and also show that I am capable of both frontend and backend software development.

# Features
V1.0

    * Add personal details, a personal summary, projects, education, work experience, skills and interests
    * Generate a CV in PDF format

V2.0

    * Stores letter content.
    * Generate cover letter to accompany CV in PDF format
    
# How to run in Linux
cd to the root directory

    * $ sudo apt install python3-virtualenv
    * $ sudo apt-get install wkhtmltopdf
    * $ virtualenv venv
    * $ source venv/bin/activate
    * $ cat requirements.txt | xargs -n 1 pip install
    * $ python3 run.py

open http://127.0.0.1:5000/ in your browser

You must fill in the 'personal details' section before trying to make a CV.

# Usage examples
(Details covered to protect information)

V1.0

Example of what each section looks like
![we1](/examples/workexperience.png)

Example of forms for additional details
![we2](/examples/workexperience1.png)

Example of the CV section
![cv1](/examples/cv.png)

Example of CV page 1
![cv2](/examples/cv1.png)

Example of CV page 2
![cv3](/examples/cv2.png)


V2.0 New Features

Example of new content for letter page
![content](/examples/content.png)

Example of new buttons to generate the letter
![genletter](/examples/generate-letter.png)

Example of Cover Letter
![letter](/examples/letter.png)
