from app import app

# Initiating the entire app.

if __name__ == '__main__':
    app.run(debug=True)
